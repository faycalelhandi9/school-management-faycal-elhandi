<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

include 'DbConnect.php';
$objDb = new DbConnect;
$conn = $objDb->connect();

$method = $_SERVER['REQUEST_METHOD'];
switch($method){
    
        
    // Rest of your code

        
    case "GET":
        $sql = "SELECT * FROM users";
        $path = explode('/', $_SERVER['REQUEST_URI']);
        if(isset($path[3]) && is_numeric($path[3])) {
            $sql .= " WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $path[3]);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else{
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        echo json_encode($users);
        break;

    case "POST":
        $user = json_decode(file_get_contents('php://input'));
        $sql = "INSERT INTO users(name, email, password) VALUES(:name, :email, :password)";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':name', $user->name);
        $stmt->bindParam(':email', $user->email);
        $stmt->bindParam(':password', $user->password);
        if($stmt->execute()){
            $response = ['status' => 1, 'message'=> 'Record created successfully'];
        } else{
            $response = ['status' => 0, 'message'=> 'Failed to create record'];
        }
        echo json_encode($response);
        break;

        case "PUT":
            $users = json_decode(file_get_contents('php://input'));
            $sql = "UPDATE users SET name=:name, email=:email, password=:password WHERE id=:id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $_GET['id']);
            $stmt->bindParam(':name', $users->name);
            $stmt->bindParam(':email', $users->email);
            $stmt->bindParam(':password', $users->password);

            if($stmt->execute()){
                $response = ['status' => 1, 'message'=> 'Record updated successfully'];
            } else{
                $response = ['status' => 0, 'message'=> 'Failed to update record'];
            }
            echo json_encode($response);
            break;

        
        
        case "DELETE":
            $path = explode('/', $_SERVER['REQUEST_URI']);
            if(isset($path[3]) && is_numeric($path[3])) {
                $id = $path[3];
                $sql = "DELETE FROM users WHERE id = :id";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':id', $id);
                if($stmt->execute()){
                    $response = ['status' => 1, 'message'=> 'Record deleted successfully'];
                } else{
                    $response = ['status' => 0, 'message'=> 'Failed to delete record'];
                }
            } else {
                $response = ['status' => 0, 'message'=> 'Invalid user ID'];
            }
            echo json_encode($response);
            break;
        
        
}

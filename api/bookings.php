<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "react";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // validate booking_date value
    if(!isset($_POST['booking_date']) || empty($_POST['booking_date'])) {
      throw new Exception("Booking date is required");
    }

    // get user_id value from users table
    $user_id = 2; // replace with your own default value

    $user_query = $conn->prepare("SELECT id FROM users WHERE email = :user_email");
    $user_query->bindParam(':user_email', $_POST['user_email']);
    $user_query->execute();
    $user_result = $user_query->fetch(PDO::FETCH_ASSOC);
    if ($user_result) {
      $user_id = $user_result['id'];
    }

    // prepare statement
    $stmt = $conn->prepare("INSERT INTO bookings (user_id, booking_date, start_time, end_time) VALUES (:user_id, :booking_date, :start_time, :end_time)");

    // bind parameters
    $stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':booking_date', $_POST['booking_date']);
    $stmt->bindParam(':start_time', $_POST['start_time']);
    $stmt->bindParam(':end_time', $_POST['end_time']);

    // execute statement
    $stmt->execute();

    // send success response
    echo "Booking added successfully";
  } elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // prepare statement
    $stmt = $conn->prepare("SELECT * FROM bookings");

    // execute statement
    $stmt->execute();

    // fetch all rows as an associative array
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // send success response with JSON-encoded data
    header('Content-Type: application/json');
    echo json_encode($result);
  }
} catch(PDOException $e) {
  // send error response
  echo "Error: " . $e->getMessage();
} catch(Exception $e) {
  // send validation error response
  echo "Error: " . $e->getMessage();
}

$conn = null;
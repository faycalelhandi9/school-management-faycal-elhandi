<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

include 'DbConnect.php';
$objDb = new DbConnect;
$conn = $objDb->connect();

$method = $_SERVER['REQUEST_METHOD'];
switch($method){
    case "POST":
        $data = json_decode(file_get_contents('php://input'), true);
        $username = $data['username'];
        $password = $data['password'];
        
        $stmt = $conn->prepare("SELECT * FROM users WHERE email = :email AND password = :password");
        $stmt->bindParam(':email', $username);
        $stmt->bindParam(':password', $password);
        $stmt->execute();
        
        if ($stmt->rowCount() > 0) {
            // Login successful, set session and redirect to ListUser
            session_start();
            $_SESSION['username'] = $username;
            $_SESSION['loggedin'] = true;
            $response = ['status' => 1, 'message'=> 'Login successful', 'redirect' => 'ListUser.js'];
        } else {
            // Login failed
            $response = ['status' => 0, 'message'=> 'Login failed'];
        }
        
        echo json_encode($response);
        break;
}
?>

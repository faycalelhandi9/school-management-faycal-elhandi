<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");


include 'DbConnect.php';
$objDb = new DbConnect;
$conn = $objDb->connect();

$method = $_SERVER['REQUEST_METHOD'];
switch($method){
    case "GET":
        $sql = "SELECT * FROM Matiere";
        $path = explode('/', $_SERVER['REQUEST_URI']);
        if(isset($path[3]) && is_numeric($path[3])) {
            $sql .= " WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $path[3]);
            $stmt->execute();
            $Matieres = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else{
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $Matieres = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        echo json_encode($Matieres);
        break;

    case "POST":
        $data = json_decode(file_get_contents('php://input'));

        if(isset($data->nomMatiere) && isset($data->typeMatiere)) {
            $nomMatiere = $data->nomMatiere;
            $typeMatiere = $data->typeMatiere;

            $sql = "INSERT INTO Matiere(id, nomMatiere, typeMatiere) VALUES(NULL, :nomMatiere, :typeMatiere)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':nomMatiere', $nomMatiere);
            $stmt->bindParam(':typeMatiere', $typeMatiere);

            if($stmt->execute()){
                $response = ['status' => 1, 'message'=> 'Record created successfully'];
            } else{
                $response = ['status' => 0, 'message'=> 'Failed to create record'];
            }
        } else {
            $response = ['status' => 0, 'message'=> 'Please provide a value for nomMatiere and typeMatiere'];
        }

        echo json_encode($response);
        break;
        case "PUT":
            $matiere = json_decode(file_get_contents('php://input'));
            $sql = "UPDATE Matiere SET nomMatiere=:nomMatiere, typeMatiere=:typeMatiere WHERE id=:id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $_GET['id']);
            $stmt->bindParam(':nomMatiere', $matiere->nomMatiere);
            $stmt->bindParam(':typeMatiere', $matiere->typeMatiere);

            if($stmt->execute()){
                $response = ['status' => 1, 'message'=> 'Record updated successfully'];
            } else{
                $response = ['status' => 0, 'message'=> 'Failed to update record'];
            }
            echo json_encode($response);
            break;
        

    case "DELETE":
        $path = explode('/', $_SERVER['REQUEST_URI']);
        if(isset($path[3]) && is_numeric($path[3])) {
            $id = $path[3];
            $sql = "DELETE FROM Matiere WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $id);

            if($stmt->execute()) {
                $response = ['status' => 1, 'message'=> 'Record deleted successfully'];
            } else {
                $response = ['status' => 0, 'message'=> 'Failed to delete record'];
            }
        } else {
            $response = ['status' => 0, 'message'=> 'Please provide a valid id'];
        }

        echo json_encode($response);
        break;
        
        }
        
        
?>
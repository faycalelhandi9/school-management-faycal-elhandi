<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

include 'DbConnect.php';
$objDb = new DbConnect;
$conn = $objDb->connect();

$method = $_SERVER['REQUEST_METHOD'];
switch($method){

    case "POST":
        $salle = json_decode(file_get_contents('php://input'));
        $sql = "INSERT INTO salles(nom_salle, numero_salle, type_salle) VALUES(:nom_salle, :numero_salle, :type_salle)";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':nom_salle', $salle->nom_salle);
        $stmt->bindParam(':numero_salle', $salle->numero_salle);
        $stmt->bindParam(':type_salle', $salle->type_salle);
        if($stmt->execute()){
            $response = ['status' => 1, 'message'=> 'Record created successfully'];
        } else{
            $response = ['status' => 0, 'message'=> 'Failed to create record'];
        }
        echo json_encode($response);
        break;

    case "GET":
        $sql = "SELECT * FROM salles";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($result);
        break;

    case "PUT":
        $salle = json_decode(file_get_contents('php://input'));
        $sql = "UPDATE salles SET nom_salle=:nom_salle, numero_salle=:numero_salle, type_salle=:type_salle WHERE id=:id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $_GET['id']);
        $stmt->bindParam(':nom_salle', $salle->nom_salle);
        $stmt->bindParam(':numero_salle', $salle->numero_salle);
        $stmt->bindParam(':type_salle', $salle->type_salle);
        if($stmt->execute()){
            $response = ['status' => 1, 'message'=> 'Record updated successfully'];
        } else{
            $response = ['status' => 0, 'message'=> 'Failed to update record'];
        }
        echo json_encode($response);
        break;

    case "DELETE":
        $path = explode('/', $_SERVER['REQUEST_URI']);
        if(isset($path[3]) && is_numeric($path[3])) {
            $id = $path[3];
            $sql = "DELETE FROM salles WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $id);
            if($stmt->execute()){
                $response = ['status' => 1, 'message'=> 'Record deleted successfully'];
            } else{
                $response = ['status' => 0, 'message'=> 'Failed to delete record'];
            }
        } else {
            $response = ['status' => 0, 'message'=> 'Invalid salle ID'];
        }
        echo json_encode($response);
        break;
}


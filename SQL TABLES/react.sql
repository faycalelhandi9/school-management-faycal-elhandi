-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 18 avr. 2023 à 04:57
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `react`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(55) DEFAULT NULL,
  `password` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('faycalelhandi9', '20032022');

-- --------------------------------------------------------

--
-- Structure de la table `bookings`
--

CREATE TABLE `bookings` (
  `booking_id` int(11) NOT NULL,
  `booking_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `bookings`
--

INSERT INTO `bookings` (`booking_id`, `booking_date`, `start_time`, `end_time`, `user_id`) VALUES
(1, '2023-04-11', '16:41:00', '05:16:00', 2),
(2, '2023-04-11', '18:55:00', '08:43:00', 2),
(3, '2023-04-11', '14:37:00', '13:00:00', 2),
(4, '2023-04-12', '19:05:00', '17:41:00', 2),
(5, '2023-04-12', '13:49:00', '17:35:00', 2),
(6, '2023-04-05', '04:49:00', '09:36:00', 2),
(7, '2023-04-12', '20:59:00', '04:41:00', 2),
(8, '2023-04-12', '21:05:00', '00:59:00', 2),
(9, '2023-04-12', '16:36:00', '02:49:00', 2),
(10, '2023-04-12', '15:40:00', '14:05:00', 2),
(11, '2023-04-13', '00:38:00', '13:53:00', 2),
(12, '2023-04-12', '02:41:00', '12:41:00', 2),
(13, '2023-04-12', '04:38:00', '09:54:00', 2),
(14, '2023-04-13', '02:07:00', '17:52:00', 2),
(15, '2023-04-13', '16:23:00', '06:43:00', 2),
(16, '1988-11-01', '06:04:00', '16:04:00', 2),
(17, '2023-04-21', '04:13:00', '11:52:00', 2),
(18, '2023-04-14', '17:35:00', '06:54:00', 2),
(19, '2023-04-14', '16:18:00', '09:16:00', 2),
(20, '2023-04-14', '02:26:00', '10:42:00', 2),
(21, '2023-04-14', '13:02:00', '02:41:00', 2),
(22, '2023-04-14', '16:10:00', '19:26:00', 2),
(23, '2023-04-14', '10:45:00', '13:03:00', 2);

-- --------------------------------------------------------

--
-- Structure de la table `Matiere`
--

CREATE TABLE `Matiere` (
  `id` int(15) NOT NULL,
  `nomMatiere` varchar(55) NOT NULL,
  `typeMatiere` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `Matiere`
--

INSERT INTO `Matiere` (`id`, `nomMatiere`, `typeMatiere`) VALUES
(16, 'MAth', 'theorique');

-- --------------------------------------------------------

--
-- Structure de la table `salles`
--

CREATE TABLE `salles` (
  `id` int(15) NOT NULL,
  `nom_salle` varchar(55) NOT NULL,
  `numero_salle` int(15) NOT NULL,
  `type_salle` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `salles`
--

INSERT INTO `salles` (`id`, `nom_salle`, `numero_salle`, `type_salle`) VALUES
(5, 'Salle informatique', 15, 'Pratique');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'Faycal El handi', 'Faycalelhandi9@gmail.com', 'SJKDSDSsd'),
(52, 'Yvonne Bender', 'ciruguvica@mailinator.com', 'Pa$$w0rd!'),
(53, 'Aiko Graves', 'fiwyr@mailinator.com', 'Pa$$w0rd!'),
(54, 'Chandler Walter', 'gyleselazo@mailinator.com', 'Pa$$w0rd!');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `Matiere`
--
ALTER TABLE `Matiere`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `salles`
--
ALTER TABLE `salles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `Matiere`
--
ALTER TABLE `Matiere`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `salles`
--
ALTER TABLE `salles`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

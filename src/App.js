import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import ListUser from './components/Users/ListUser';
import CreateUser from './components/Users/CreateUser';
import LoginPage from './components/Users/LoginPage';
import EditUser from './components/Users/EditUser';
import Homepage from './components/Users/Homepage';
import Dashboard from './components/Users/Dashboard';
import CreateMatiere from './components/Matiere/CreateMatiere';
import ListMatiere from './components/Matiere/ListMatiere';
import EditMatiere from './components/Matiere/EditMatiere';
import CreateSalle from './components/Salle/CreateSalle';
import ListSalle from './components/Salle/ListSalle';
import EditSalle from './components/Salle/EditSalle';
import AvailabilityForm from './components/Professor Dashboard/AvailabilityForm';
import LoginProfessor from './components/Professor Dashboard/LoginProfessor'
import axios from 'axios';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <header className="App-header">
      
      </header>

      
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container-fluid">
    
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav">
        <li className="nav-item">
          
        </li>
        <li className="nav-item">
          
        </li>
      </ul>
    </div>
  </div>
</nav>

        <Routes>
        <Route path='/' element={<Homepage />} />

          <Route path='/user/loginpage' element={<LoginPage />} />
          <Route path="/user" element={<ListUser />} />
          <Route path="/user/create" element={<CreateUser />} />
          <Route path="user/:id/edit" element={<EditUser/>}/>
          <Route path='dashboard' element={<Dashboard/>}/>
          <Route path='matiere/create' element={<CreateMatiere/>}/>
          <Route path='matiere' element={<ListMatiere/>}/>
          <Route path='matiere/:id/edit' element={<EditMatiere/>} />
          <Route path='Salle/create' element={<CreateSalle/>}/>
          <Route path='Salle' element={<ListSalle/>}/>
          <Route path='Salle/:id/edit' element={<EditSalle/>}/>
          <Route path='Available' element={<AvailabilityForm/>}/>
          <Route path='Professor/login' element={<LoginProfessor/>}/>

           
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

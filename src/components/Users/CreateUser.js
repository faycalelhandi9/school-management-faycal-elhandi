import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function CreateUser (){

    const navigate = useNavigate();

    const [inputs,  setInputs] = useState({})

    const handleChange = (event)=> {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}));
    }

    const handleSubmit = (event) =>{
        event.preventDefault();

        axios.post('http://localhost/api/user/save', inputs).then(function(response){
            console.log(response.data)
            navigate('/user');
        });
        console.log(inputs);
    }

    return (
        <div className="container">
            <h1> <Link to="/dashboard" className="btn-lg btn-dark">        EPG School
</Link></h1>
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                            <h1 className="text-center">Dashboard admin</h1>
                        </div>
                        <div className="card-body">
                            <h5>Create Users</h5>
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="name">Name:</label>
                                    <input type="text" className="form-control" id="name" name="name" onChange={handleChange} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email:</label>
                                    <input type="email" className="form-control" id="email" name="email" onChange={handleChange} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password:</label>
                                    <input type="password" className="form-control" id="password" name="password" onChange={handleChange} />
                                </div>
                                <button type="submit" className="btn btn-primary">Create a user</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateUser;

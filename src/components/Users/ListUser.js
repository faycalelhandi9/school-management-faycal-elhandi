import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function ListUser() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  function getUsers() {
    axios
      .get("http://localhost/api/user/save/")
      .then(function (response) {
        console.log(response.data);
        setUsers(response.data);
      });
  }

  const handleDelete = (userId) => {
    confirmAlert({
      title: 'Confirmation',
      message: 'Êtes-vous sûr de vouloir supprimer cet utilisateur ?',
      buttons: [
        {
          label: 'Oui',
          onClick: () => {
            axios
              .delete(`http://localhost/api/user/${userId}/delete`)
              .then(function (response) {
                console.log(response.data);
                const updatedUsers = users.filter((user) => user.id !== userId);
                setUsers(updatedUsers);
              });
          },
        },
        {
          label: 'Non',
          onClick: () => {},
        },
      ],
    });
  };

  return (
    <div className="container-fluid bg-white text-dark py-3">
      <h1><Link to="/dashboard" className="btn-lg btn-dark text-light">EPG School</Link></h1>
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h1>List Users</h1>
        <Link to="/user/create" className="btn btn-dark">
          Create User
        </Link>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, key) => (
            <tr key={key}>
              <td>{user.id}</td>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.password}</td>
              <td>
                <Link to={`${user.id}/edit`} className="btn btn-dark">
                  Edit
                </Link>
                <button
                  className="btn btn-light text-dark"
                  onClick={() => handleDelete(user.id)}
                >
                  Delete
                </button>
                
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ListUser;

import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Swal from 'sweetalert2';


function EditUser() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`http://localhost/api/index.php?id=${id}`)
      .then(function (response) {
        console.log(response.data);
        setName(response.data.name);
        setEmail(response.data.email);
        setPassword(response.data.password);
      });
  }, [id]);

  function handleSubmit(event) {
  event.preventDefault();

  const user = {
    name: name,
    email: email,
    password: password,
  };

  axios.put(`http://localhost/api/index.php?id=${id}`, user)
    .then(function (response) {
      console.log(response.data);
      Swal.fire({
        title: "Success!",
        text: "User has been updated.",
        icon: "success",
        confirmButtonText: "OK",
      }).then(function () {
        window.location.href = "/user";
      });
    })
    .catch(function (error) {
      console.log(error);
      Swal.fire({
        title: "Error!",
        text: "Failed to update user.",
        icon: "error",
        confirmButtonText: "OK",
      });
    });
}

  return (
    <div className="container">
      <h1>
        <Link to="/dashboard" className="btn-lg btn-dark">
          EPG School
        </Link>
      </h1>
      <h1>Edit User</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            className="form-control"
            id="name"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            className="form-control"
            id="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            className="form-control"
            id="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Update User
        </button>
      </form>
    </div>
  );
}

export default EditUser;

import React from 'react';
import { Link } from 'react-router-dom';
import LoginPage from './LoginPage';

function Homepage() {
  return (
    <div className="container">
      <div className="jumbotron">
        <h1>Welcome to EPG School</h1>
        <p>Best place to learn and grow</p>
      </div>
      <div className="row">
        <div className="col-md-6">
          <Link to="/user/loginpage" className="btn btn-dark btn-lg btn-block">
            Login for Admin
          </Link>
        </div>
        <div className="col-md-6">
          <Link to="/Professor/login" className="btn btn-dark btn-lg btn-block">
            Login for Professor
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Homepage;

import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Homepage from "./Homepage";
import { Link } from "react-router-dom";

function LoginPage() {
  const navigate = useNavigate();

  const [inputs, setInputs] = useState({});

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    axios
      .post("http://localhost/api/index2.php", inputs)
      .then(function (response) {
        console.log(response.data);
        if (response.data.status) {
          // Login successful, redirect to ListUser.js
          window.location.href = "/dashboard";
        } else {
          // Login failed, display error message
          alert(response.data.message);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };


  return (
    <div className="container">
      <h1> <Link to="/" className="btn-lg btn-dark">        EPG School
            </Link></h1>
      <div className="row justify-content-center">
        <div className="col-md-6">
          <div className="card">
            <div className="card-header">
            
              <h1 className="text-center">Login</h1>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <label htmlFor="username">Username:</label>
                  <input
                    type="text"
                    className="form-control"
                    id="username"
                    name="username"
                    onChange={handleChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password:</label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    name="password"
                    onChange={handleChange}
                  />
                </div>
                <button type="submit" className="btn btn-primary">
                  Login
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;

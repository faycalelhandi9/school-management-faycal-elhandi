import React from 'react';
import { Link } from 'react-router-dom';

function Dashboard() {
  const handleLogout = () => {

    localStorage.removeItem('user');
    window.location.href = '/';

  };

  return (
    <div className="container-fluid bg-white text-dark py-3">
      <h1 className="text-center mb-0">EPG School</h1>
      <button onClick={handleLogout} className="btn btn-dark float-end text-white">Logout</button>
      <div className="jumbotron bg-white">
        <h1>Welcome to the EPG School Dashboard</h1>
        <p>Here you can manage your school's resources</p>
      </div>
      <div className="row">
        <div className="col-md-4">
          <Link to="/Salle/create" className="btn btn-dark btn-lg btn-block mb-3">
            Create Salle
          </Link>
          <Link to="/Salle" className="btn btn-dark btn-lg btn-block">
            List Salle
          </Link>
        </div>
        <div className="col-md-4">
          <Link to="/user/create" className="btn btn-dark btn-lg btn-block mb-3">
            Create User
          </Link>
          <Link to="/user" className="btn btn-dark btn-lg btn-block">
            List Users
          </Link>
        </div>
        <div className="col-md-4">
          <Link to="/matiere/create" className="btn btn-dark btn-lg btn-block mb-3">
            Create Matiere
          </Link>
          <Link to="/matiere" className="btn btn-dark btn-lg btn-block">
            List Matiere
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

function ListSalle() {
  const [salles, setSalles] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost/api/salle1.php")
      .then((response) => {
        setSalles(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleDelete = (id) => {
    confirmAlert({
      title: 'Confirmation',
      message: 'Êtes-vous sûr de vouloir supprimer cette salle ?',
      buttons: [
        {
          label: 'Oui',
          onClick: () => {
            axios
              .delete(`http://localhost/api/salle1.php/${id}`)
              .then((response) => {
                setSalles(salles.filter((salle) => salle.id !== id));
              })
              .catch((error) => {
                console.log(error);
              });
          },
        },
        {
          label: 'Non',
          onClick: () => {},
        },
      ],
    });
  };

  return (
    <div className="container-fluid bg-white text-dark">
      <h1>
        <Link to="/dashboard" className="btn-lg btn-dark text-light">
          EPG School
        </Link>
      </h1>
      <h1 className="my-5 text-center">Liste des salles</h1>
      <table className="table table-bordered table-striped">
        <thead>
          <tr>
            <th className="bg-dark text-light">Nom de la salle</th>
            <th className="bg-dark text-light">Numéro de la salle</th>
            <th className="bg-dark text-light">Type de salle</th>
            <th className="bg-dark text-light">Actions</th>
          </tr>
        </thead>
        <tbody>
          {salles.map((salle) => (
            <tr key={salle.id}>
              <td>{salle.nom_salle}</td>
              <td>{salle.numero_salle}</td>
              <td>{salle.type_salle}</td>
              <td>
                <Link
                  to={`/salle/${salle.id}/edit`}
                  className="btn btn-sm btn-dark mr-2"
                >
                  Modifier
                </Link>
                <button
                  onClick={() => handleDelete(salle.id)}
                  className="btn btn-sm btn-dark"
                >
                  Supprimer
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ListSalle;

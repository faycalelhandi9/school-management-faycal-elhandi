import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { Link } from "react-router-dom";

function EditSalle() {
  const [nomSalle, setNomSalle] = useState("");
  const [numeroSalle, setNumeroSalle] = useState("");
  const [typeSalle, setTypeSalle] = useState("");

  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`http://localhost/api/salle1.php?id=${id}`)
      .then((response) => {
        const salle = response.data;
        setNomSalle(salle.nom_salle);
        setNumeroSalle(salle.numero_salle);
        setTypeSalle(salle.type_salle);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();

    const salle = {
      nom_salle: nomSalle,
      numero_salle: numeroSalle,
      type_salle: typeSalle,
    };

    confirmAlert({
      title: 'Confirmation',
      message: 'Are you sure you want to update this salle?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            axios
              .put(`http://localhost/api/salle1.php?id=${id}`, salle)
              .then((response) => {
                window.location.href = "/salle";
              })
              .catch((error) => {
                console.log(error);
              });
          }
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
    });
  };

  return (
    <div className="container">
      <h1> <Link to="/dashboard" className="btn-lg btn-dark">EPG School</Link></h1>
      <h1>Modifier une salle</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nomSalle" >Nom de la salle</label>
          <input
            type="text"
            className="form-control"
            id="nomSalle"
            value={nomSalle}
            onChange={(event) => setNomSalle(event.target.value)}
            placeholder="Entrer Nom de salle"
          />
        </div>
        <div className="form-group">
          <label htmlFor="numeroSalle">Numéro de la salle</label>
          <input
            type="Number"
            className="form-control"
            id="numeroSalle"
            value={numeroSalle}
            onChange={(event) => setNumeroSalle(event.target.value)}
            placeholder="Entrer Numero de salle"
          />
        </div>
        <div className="form-group">
          <label htmlFor="typeSalle">Type de salle</label>
          <select
            className="form-control"
            id="typeSalle"
            value={typeSalle}
            onChange={(event) => setTypeSalle(event.target.value)}
          >
            <option value="Pratique">Pratique</option>
            <option value="Théorique">Théorique</option>
          </select>
        </div>

        <button type="submit" className="btn btn-primary">
          Enregistrer
        </button>
      </form>
    </div>
  );
}

export default EditSalle;

import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function CreateSalle(){

    const navigate = useNavigate();

    const [inputs, setInputs] = useState({});

    const handleChange = (event) => {
        const id = event.target.id;
        const value = event.target.value;
        setInputs(values => ({...values, [id]: value}));
    }
    

    const handleSubmit = (event) => {
        event.preventDefault();

        axios.post('http://localhost/api/salle1.php', inputs)
        .then(function(response){
            console.log(response.data);
            navigate('/salle');
        })
        .catch(function(error){
            console.log(error);
        });
        console.log(inputs);
    }
    
      

    return (
        <div className="container">
            <h1>
                <Link to="/dashboard" className="btn-lg btn-dark">EPG School</Link>
            </h1>
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                            <h1 className="text-center">Dashboard admin</h1>
                        </div>
                        <div className="card-body">
                            <h5>Create Salle</h5>
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="nom_salle">Nom Salle:</label>
                                    <input type="text" className="form-control" id="nom_salle" name="nom_salle" onChange={handleChange} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="numero_salle">Numero Salle:</label>
                                    <input type="Number" className="form-control" id="numero_salle" name="numero_salle" onChange={handleChange} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="type_salle">Type Salle:</label>
                                    <select className="form-control" id="type_salle" name="type_salle" onChange={handleChange}>
                                        <option value="">-- Select a Type --</option>
                                        <option value="pratique">Pratique</option>
                                        <option value="theorique">Theorique</option>
                                    </select>
                                </div>
                                <button type="submit" className="btn btn-primary btn-block">Create Salle</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateSalle;

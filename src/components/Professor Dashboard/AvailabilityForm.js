import React, { useState, useEffect } from 'react';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import Modal from "react-modal";
import "./modal.css";
import axios from 'axios';

const handleLogout = () => {
  localStorage.removeItem('user');
  window.location.href = '/';
};

function AvailabilityForm() {
  const [bookingDate, setBookingDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [selectedDate, setSelectedDate] = useState(null);
  const [isOpen, setIsOpen] = useState(false);
  const [events, setEvents] = useState([]);
  const [bookings, setBookings] = useState([]);

  useEffect(() => {
    getBookings();
  }, []);

  function getBookings() {
    axios.get("http://localhost/api/bookings.php")
      .then(function (response) {
        setBookings([...response.data]);
        setEvents(getEventsFromBookings(response.data));
      });
  }

  function getEventsFromBookings(bookings) {
    return bookings.map((booking) => ({
      title: 'Booked',
      start: new Date(booking.start_time),
      end: new Date(booking.end_time),
      backgroundColor: "#FFC107",
      borderColor: "#FFC107"
    }));
  }

  const handleDateClick = (clickInfo) => {
    setSelectedDate(clickInfo.date);
    setBookingDate(clickInfo.dateStr);
    setIsOpen(true);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!bookingDate) {
      alert("Booking date is required");
      return;
    }

    const formData = new FormData();
    formData.append('booking_date', bookingDate);
    formData.append('start_time', startTime);
    formData.append('end_time', endTime);

    fetch('http://localhost/api/bookings.php', {
      method: 'POST',
      body: formData
    })
      .then(response => response.text())
      .then(data => {
        alert(data);
        setSelectedDate(null);
        setIsOpen(false);
        getBookings();
      })
      .catch(error => console.log(error));
  };

  const getEventColor = (event) => {
    const booking = bookings.find(booking =>
      booking.start_time === event.start.toISOString()
    );
    return booking ? '#FFC107' : '#3788d8';
  };


return (
<div className="CalendarBig">

<button onClick={handleLogout} className="btn btn-dark text-white">Logout</button>
<FullCalendar
plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
initialView={"dayGridMonth"}
headerToolbar={{
start: "today prev,next",
center: "title",
end: "",
}}

height="90vh"
width="75vw"
dateClick={handleDateClick}
selectable={true}
selectOverlap={false}
eventOverlap={false}
allDaySlot={false}
select={(selectionInfo) => {
handleDateClick(selectionInfo);
}}
events={events}
/>


<Modal
isOpen={isOpen}
className="modal-content"
overlayClassName="modal-overlay"
>
<form onSubmit={handleSubmit}>
<label htmlFor="bookingDate">Session Date</label>
<p>{selectedDate ? selectedDate.toDateString() : ""}</p>

<label htmlFor="startTime">Start Time</label>
<input
type="time"
id="startTime"
name="start_time"
value={startTime}
onChange={(e) => setStartTime(e.target.value)}
/>

<label htmlFor="endTime">End Time</label>
<input
type="time"
id="endTime"
name="end_time"
value={endTime}
onChange={(e) => setEndTime(e.target.value)}
/>

<button type="submit">Submit</button>
</form>
</Modal>
<div>
<table className="table">
<thead>
<tr>
<th>#</th>
<th>date</th>
<th>start</th>
<th>end</th>
</tr>
</thead>
<tbody>
{bookings.map((booking, key) => (
<tr key={key}>
<td>{booking.booking_id}</td>
<td>{booking.booking_date}</td>
<td>{booking.start_time}</td>
<td>{booking.end_time}</td>
</tr>
))}
</tbody>
</table>
</div>
</div>
);


}

export default AvailabilityForm;


import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function LoginProfessor() {
  const navigate = useNavigate();

  const [inputs, setInputs] = useState({});
  const [errorMessage, setErrorMessage] = useState("");

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    axios
      .post("http://localhost/api/LoginProfessor.php", inputs)
      .then(function (response) {
        console.log(response.data);
        if (response.data.status) {
          // Login successful, redirect to ListUser.js
          window.location.href = "/Available";
        } else {
          // Login failed, display error message
          setErrorMessage("Email or password is incorrect. Please try again.");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };


  return (
    <div className="container">
      <h1> <Link to="/" className="btn-lg btn-block">        EPG School
            </Link></h1>
      <div className="row justify-content-center">
        <div className="col-md-6">
          <div className="card">
            <div className="card-header">
            
              <h1 className="text-center">Login</h1>
            </div>
            <div className="card-body">
              {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <label htmlFor="username">Username:</label>
                  <input
                    type="text"
                    className="form-control"
                    id="username"
                    name="username"
                    onChange={handleChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password:</label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    name="password"
                    onChange={handleChange}
                  />
                </div>
                <button type="submit" className="btn btn-primary">
                  Login
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginProfessor;

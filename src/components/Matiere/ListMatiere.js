import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';


function ListMatiere() {
  const [matieres, setMatieres] = useState([]);

  useEffect(() => {
    getMatieres();
  }, []);

  function getMatieres() {
    axios.get("http://localhost/api/index3.php")

      .then(function (response) {
        console.log(response.data);
        setMatieres(response.data);
      });
  }

  function handleDelete(matiereId) {
    confirmAlert({
      title: 'Confirmation',
      message: 'Êtes-vous sûr de vouloir supprimer cette matière ?',
      buttons: [
        {
          label: 'Oui',
          onClick: () => {
            axios
              .delete(`http://localhost/api/index3.php/${matiereId}`)
              .then(function (response) {
                console.log(response.data);
                const updatedMatieres = matieres.filter(
                  (matiere) => matiere.id !== matiereId
                );
                setMatieres(updatedMatieres);
              });
          },
        },
        {
          label: 'Non',
          onClick: () => {},
        },
      ],
    });
  }


  return (
    <div className="container">
      <h1>
        <Link to="/dashboard" className="btn-lg btn-dark">
          EPG School
        </Link>
      </h1>
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h1>List Matieres</h1>
        <Link to="/matiere/create" className="btn btn-dark text-light">
          Create Matiere
        </Link>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {matieres.map((matiere, key) => (
            <tr key={key}>
              <td>{matiere.id}</td>
              <td>{matiere.nomMatiere}</td>
              <td>{matiere.typeMatiere}</td>
              <td>
                <Link to={`${matiere.id}/edit`} className="btn btn-dark">
                  Edit
                </Link>
                <button
                  className="btn btn-dark"
                  onClick={() => handleDelete(matiere.id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ListMatiere;

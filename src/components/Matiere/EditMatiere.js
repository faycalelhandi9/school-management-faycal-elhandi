import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Swal from 'sweetalert2';
import './EditMatiere.css';


function EditMatiere() {
  const [nomMatiere, setNomMatiere] = useState("");
  const [typeMatiere, setTypeMatiere] = useState("theorique");

  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`http://localhost/api/index3.php?id=${id}`)
      .then(function (response) {
        console.log(response.data);
        setNomMatiere(response.data.nomMatiere);
        setTypeMatiere(response.data.typeMatiere);
      });
  }, [id]);

  function handleSubmit(event) {
    event.preventDefault();
  
    const matiere = {
      nomMatiere: nomMatiere,
      typeMatiere: typeMatiere,
    };
  
    axios
      .put(`http://localhost/api/index3.php?id=${id}`, matiere)
      .then(function (response) {
        console.log(response.data);
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Matiere updated successfully!",
        }).then(() => {
          window.location.href = "/matiere";
        });
      })
      .catch(function (error) {
        console.log(error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "An error occurred while updating the matiere.",
        });
      });
  }

  return (
    <div className="container">
        <h1>
        <Link to="/dashboard" className="btn-lg btn-dark">
          EPG School
        </Link>
      </h1>
      <h1>Edit Matiere</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nomMatiere">Nom Matiere:</label>
          <input
            type="text"
            className="form-control"
            id="nomMatiere"
            value={nomMatiere}
            onChange={(event) => setNomMatiere(event.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="typeMatiere">Type Matiere:</label>
          <select
            className="form-control"
            id="typeMatiere"
            value={typeMatiere}
            onChange={(event) => setTypeMatiere(event.target.value)}
          ><option value="">-- Select a Type --</option>
            <option value="theorique">Theorique</option>
            <option value="pratique">Pratique</option>
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Update Matiere
        </button>
      </form>
    </div>
  );
}

export default EditMatiere;

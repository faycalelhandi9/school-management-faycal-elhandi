import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function CreateMatiere(){

    const navigate = useNavigate();

    const [inputs,  setInputs] = useState({})

    const handleChange = (event)=> {
        const id = event.target.id;
        const value = event.target.value;
        setInputs(values => ({...values, [id]: value}));
    }
    

    const handleSubmit = (event) =>{
        event.preventDefault();

        axios.post('http://localhost/api/index3.php', inputs).then(function(response){
            console.log(response.data)
            navigate('/matiere');
        });
        console.log(inputs);
    }
    
      

    return (
        <div className="container">
        <h1> <Link to="/dashboard" className="btn-lg btn-dark">EPG School</Link></h1>
        <div className="row justify-content-center">
            <div className="col-md-6">
                <div className="card">
                    <div className="card-header">
                        <h1 className="text-center">Dashboard admin</h1>
                    </div>
                    <div className="card-body">
                        <h5>Create Matiere</h5>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="nomMatiere">Nom Matiere:</label>
                                <input type="text" className="form-control" id="nomMatiere" name="nomMatiere" onChange={handleChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="typeMatiere">Type Matiere:</label>
                                <select className="form-control" id="typeMatiere" name="typeMatiere" onChange={handleChange}>
                                    <option value="">-- Select a Type --</option>
                                    <option value="pratique">Pratique</option>
                                    <option value="theorique">Theorique</option>
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Create Matiere</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
)
}

export default CreateMatiere;
